{ pkgs, ruby_3_1, bundlerEnv, mkShell }:
let
  env = bundlerEnv {
    name = "realbnb-bundler-env";
    ruby = ruby_3_1;
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
  };
in
mkShell {
  name = "realbnb-shell";
  buildInputs = with pkgs; [
    env
    postgresql_13
    env.wrappedRuby
    nodejs-16_x
    bundix
    bundler
  ];
}
